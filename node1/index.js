var express = require('express');
var app = express();

app.use(require('body-parser').json());

app.post('/forceRedirect', function (req, res, next) {
	var dataRecieved = {
		requestHeaderVersion:req.get('version'),
		requestBodyParam1:req.body.firstname,
    requestBodyParam2:req.body.lastname,
		path:req.originalUrl
	};
	
    console.log('Data logging on node app 1: ' + JSON.stringify(dataRecieved));        
    return res.redirect(301, 'http://localhost:4000/test/' +   encodeURIComponent(JSON.stringify(dataRecieved)));
    //return res.redirect(301, 'http://172.104.53.62/test/' + dataRecieved); // Call node app 2 on another domain
    // Below is to test redirection on local node app 2    
})

app.all('/test/:dataRecieved', function(req, res){
   console.log('Data on same app but diff. middleware :' + encodeURIComponent(JSON.stringify(dataRecieved)))
   res.end();  
});


app.listen(3000, function () {
  console.log('Node 1 listening on port 3000!')
})