# API Gateway (Node express app) #


### What is this repository for? ###

This is a POC for sending payload in body and request header from one node express app to another that are running both locally OR one locally and another on a domain. We use routing parameter and a 301 redirection for doing this.

*Note:* **301** redirection will lead to the original content into a GET request while **307** to a POST.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Adnan F. Baliwala